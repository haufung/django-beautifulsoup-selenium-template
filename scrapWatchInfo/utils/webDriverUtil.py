import os

from selenium import webdriver


def webDriverSetup():
    dir_path = os.path.dirname(os.path.realpath(__file__))

    options = webdriver.ChromeOptions()
    options.add_experimental_option("excludeSwitches", ["ignore-certificate-errors"])
    options.add_argument('--disable-gpu')
    options.add_argument("start-maximized")  # open Browser in maximized mode
    options.add_argument("disable-infobars")  # disabling infobars
    options.add_argument("--disable-extensions")  # disabling extensions
    options.add_argument("--disable-dev-shm-usage")  # overcome limited resource problems
    options.add_argument("--no-sandbox")  # Bypass OS security model
    # below for linux
    # options.add_argument('--headless')
    # options.add_argument("--no-sandbox")
    # options.add_argument("--disable-dev-shm-usage")
    # chrome_driver_path = dir_path + r'/chromedriver'
    # below for window
    options.binary_location = "C:\Program Files\Google\Chrome\Application\chrome.exe"
    chrome_driver_path = dir_path + r'\chromedriver-win.exe'
    return options, chrome_driver_path
