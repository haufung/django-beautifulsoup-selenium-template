from django.urls import path

from . import views

urlpatterns = [
    path('health/', views.healthCheck, name='health'),
    path('watchFamilyUrl/', views.scrapWatchFamilyUrl, name='scrapWatchFamilyUrl'),
    path('scrapUrlToTxt/', views.scrap_url_to_txt, name='scrap_url_to_txt'),
    path('parseTxtAndExtract/', views.parse_txt_and_extract, name='parse_txt_and_extract'),
    path('watchItemUrl/', views.scrapWatchItemUrl, name='scrapWatchItemUrl'),
    path('watchItemInfo/', views.scrapWatchItemInfo, name='scrapWatchItemInfo'),
    path('watchItemHeader/', views.scrapWatchItemHeader, name='scrapWatchItemHeader'),
    path('downloadImage/', views.downloadImage, name='downloadImage'),
    path('extractCsv/', views.extractCsv, name='extractCsv'),
]
