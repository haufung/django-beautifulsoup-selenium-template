import os
import shutil  # to save it locally
import time
import csv

import requests  # to get image from the web
# from PIL import Image
from bs4 import BeautifulSoup
from django.db import connection
# from nordvpn_switcher import rotate_VPN
from selenium import webdriver

from scrapWatchInfo.utils import webDriverUtil, seleniumUtil

cur = connection.cursor()


def scrapWatchFamilyUrl(brand):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        cur.execute('select brand, url from watch_brand_url where brand = %s', [brand])
        resultList = cur.fetchall()
        brand = resultList[0][0]
        url = resultList[0][1]
        driver.get(url)
        watchFamilyCss = '#content > div.family-box.row > div > div > h2 > a'
        seleniumUtil.checkLoading(driver, watchFamilyCss)
        soup = BeautifulSoup(driver.page_source, 'html.parser')
        listOfWatchFamily = soup.select(watchFamilyCss)
        for family in listOfWatchFamily:
            familyName = family['href'].split("/")[-1]
            cur.execute('insert into watch_family_url ("brand", "family", "url") VALUES (%s,%s,%s)', [brand, familyName, family['href']])

        driver.quit()
        connection.close()
    except Exception as e:
        raise e

def scrap_url_to_txt():
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        driver.get("https://whiskyauctioneer.com/auction-search?text=Glenturret&sort=field_reference_field_end_date+DESC&items_per_page=500")
        item_url_selector = '#homepage-product-listing > div > div > div.view-content > div > a'
        seleniumUtil.checkLoading(driver, item_url_selector)
        with open('readme_link.txt', 'w') as f:
            while True:
                soup = BeautifulSoup(driver.page_source, 'html.parser')
                next_page_button_selector = '#homepage-product-listing > div > div > div:nth-child(4) > ul > li.pager-next'
                next_page_button = soup.select(next_page_button_selector)
                list_of_item_url = soup.select(item_url_selector)
                for url in list_of_item_url:
                    url_string = url['href']
                    f.writelines(url_string + '\n')
                if len(next_page_button) == 0:
                    break
                driver.find_element_by_css_selector(next_page_button_selector).click()
            time.sleep(10)

        driver.quit()
        connection.close()
    except Exception as e:
        raise e

def parse_txt_and_extract():
    try:
        with open('data1_url.txt') as f:
            with open('Glenturret', 'w', newline="") as csv_file:
                csvwriter = csv.writer(csv_file)
                header = ['lot', 'name', 'currency, current bid', 'winning bid', 'end date', 'distillery', 'age', 'vintage', 'region', 'bottler', 'cask type', 'bottle strength', 'bottle size', 'distillery status', 'link']
                csvwriter.writerow(header)
                lines = f.readlines()
                options, chrome_driver_path = webDriverUtil.webDriverSetup()
                driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
                for ln in lines:
                    ln = ln.replace("\n", "")
                    driver.get(ln)
                    lot_selector = '#new-layout > div.box-outer > div.left > div.left-heading > lot'
                    seleniumUtil.checkLoading(driver, lot_selector)
                    soup = BeautifulSoup(driver.page_source, 'html.parser')
                    lot = soup.select(lot_selector)[0].text.replace(" ", "").replace("Lot:", "")

                    name_selector = '#new-layout > div.box-outer > div.right > div.left-heading > h1'
                    name = soup.select(name_selector)[0].text

                    currency = 'British Pounds'

                    current_bid_title_selector = '#new-layout > div.box-outer > div.right > div:nth-child(2) > div.amount > div'
                    winner_bid_selector = '#new-layout > div.box-outer > div.right > div.place-bid.bid-section.bid-info > div > span'
                    current_bid_selector = '#new-layout > div.box-outer > div.right > div:nth-child(2) > div.amount > div > span'
                    if len(soup.select(current_bid_title_selector)) == 0:
                        winning_bid, current_bid = soup.select(winner_bid_selector)[0].text.replace("£", "").replace(" ", ""), ""
                    else:
                        current_bid, winning_bid = soup.select(current_bid_selector)[0].text.replace("£", ""), ""

                    distillery_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-distillery.field-type-text.field-label-above > div.field-items > div'
                    distillery = ''
                    if len(soup.select(distillery_selector)) > 0:
                        distillery = soup.select(distillery_selector)[0].text

                    age_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-age.field-type-text.field-label-above > div.field-items > div'
                    age = ''
                    if len(soup.select(age_selector)):
                        age = soup.select(age_selector)[0].text

                    vintage_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-vintage.field-type-text.field-label-above > div.field-items > div'
                    vintage = ''
                    if len(soup.select(vintage_selector)):
                        vintage = soup.select(vintage_selector)[0].text

                    region_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-region.field-type-text.field-label-above > div.field-items > div'
                    region = ''
                    if len(soup.select(region_selector)):
                        region = soup.select(region_selector)[0].text

                    bottler_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-bottler.field-type-text.field-label-above > div.field-items > div'
                    bottler = ''
                    if len(soup.select(bottler_selector)):
                        bottler = soup.select(bottler_selector)[0].text

                    cask_type_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-cask-type.field-type-text.field-label-above > div.field-items > div'
                    cask_type = ''
                    if len(soup.select(cask_type_selector)):
                        cask_type = soup.select(cask_type_selector)[0].text

                    bottled_strength_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-strength.field-type-text.field-label-above > div.field-items > div'
                    bottled_strength = ''
                    if len(soup.select(bottled_strength_selector)):
                        bottled_strength = soup.select(bottled_strength_selector)[0].text

                    bottle_size_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-bottle-size.field-type-text.field-label-above > div.field-items > div'
                    bottle_size = ''
                    if len(soup.select(bottle_size_selector)):
                        bottle_size = soup.select(bottle_size_selector)[0].text

                    distillery_status_selector = '#new-layout > div.productbuttom > div.left > div.topvbn > div > div.field.field-name-field-bottle-status1.field-type-taxonomy-term-reference.field-label-above > div.field-items > div'
                    distillery_status = ''
                    if len(soup.select(distillery_status_selector)):
                        distillery_status = soup.select(distillery_status_selector)[0].text

                    csv_row = [lot, name, currency, current_bid, winning_bid, "", distillery, age, vintage, region, bottler, cask_type, bottled_strength, bottle_size, distillery_status, ln]
                    csvwriter.writerow(csv_row)

    except Exception as e:
        raise e

def scrapWatchItemUrl(brand, family):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        if family is None:
            cur.execute('select url, family from watch_family_url where brand = %s and family not in (select distinct(family) from watch_item_url  where brand = %s)', [brand,brand])
        else:
            cur.execute('select url from watch_family_url where brand = %s and family = %s', [brand, family])
        resultList = cur.fetchall()
        for result in resultList:
            if len(result) > 1:
                family = result[1]
            driver.get(result[0])
            watchItemCss = '#content > div.watch-block-container > a'
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            listOfWatch = soup.select(watchItemCss)
            for watch in listOfWatch:
                cur.execute('insert into watch_item_url ("key", "brand", "family", "url") VALUES (%s,%s,%s,%s)', [watch.select('strong')[0].getText().strip('\t\r\n'),
                                                                                                                  brand,
                                                                                                                  family,
                                                                                                                  watch['href']])
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def scrapWatchItemHeader(brand):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        cur.execute('select url from watch_item_url where brand = %s', [brand])
        listOfUrl = cur.fetchall()
        imageCss = "#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img"
        for url in listOfUrl:
            driver.get(url[0])
            seleniumUtil.checkLoading(driver, imageCss)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            headerSession = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> th")
            for header in headerSession:
                headerString = header.getText()
                strippedHeader = headerString.strip('\t\r\n:')
                cur.execute(f"select count(*) from watch_item_header_map where session = 'header' and key = %s and brand = %s", [strippedHeader, brand])
                count = cur.fetchall()
                webHeader = "header_" + strippedHeader
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedHeader, "header", uid, brand, webHeader])

            caseSession = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > th")
            for case in caseSession:
                caseString = case.getText()
                strippedCase = caseString.strip('\t\r\n:')
                cur.execute("select count(*) from watch_item_header_map where session = 'case' and key = %s and brand = %s", [strippedCase, brand])
                count = cur.fetchall()
                webHeader = "case_" + strippedCase
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedCase, "case", uid, brand, webHeader])
            dialSession = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > th")
            for dial in dialSession:
                dialString = dial.getText()
                strippedDial = dialString.strip('\t\r\n:')
                cur.execute("select count(*) from watch_item_header_map where session = 'dial' and key = %s and brand = %s", [strippedDial, brand])
                count = cur.fetchall()
                webHeader = "dial_" + strippedDial
                uid = webHeader + "_" + brand
                if count[0][0] == 0:
                    cur.execute('''INSERT INTO watch_item_header_map("key", "session", "timestamp", "uid", "brand", "web_header") VALUES (%s,%s,NOW(),%s,%s,%s)''', [strippedDial, "dial", uid, brand, webHeader])
            cur.execute(
                '''update watch_item_header_map w1 set table_header = w2.table_header from watch_item_header_map w2 where w1.key = w2.key and w1.session = w2.session and w1.brand = %s and w2.brand != %s''',
                [brand, brand])
            connection.commit()
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def scrapWatchItemInfo(brand, family):
    try:
        options, chrome_driver_path = webDriverUtil.webDriverSetup()
        driver = webdriver.Chrome(options=options, executable_path=chrome_driver_path)
        if family is None:
            cur.execute('select key, url, family from watch_item_url where brand = %s and url not in (select info_url from watch_item_info)', [brand])
        else:
            cur.execute('select key, url, family from watch_item_url where brand = %s and family = %s and url not in (select info_url from watch_item_info)', [brand, family])
        listOfResult = cur.fetchall()
        imageCss = "#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img"
        cur.execute('select web_header, table_header from watch_item_header_map where brand = %s', [brand])
        watchItemHeaderMap = dict(cur.fetchall())
        for result in listOfResult:
            dbColumnList = []
            dbValueList = []
            if len(result) > 2:
                family = result[2]
            driver.get(result[1])
            seleniumUtil.checkLoading(driver, imageCss)
            soup = BeautifulSoup(driver.page_source, 'html.parser')
            headerSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> th")
            headerSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > table > tbody > tr> td")
            for index, headerKey in enumerate(headerSessionKey):
                cleanHeaderKey = headerKey.getText().strip('\t\r\n:')
                cleanHeaderValue = headerSessionValue[index].getText().strip().replace("\n", " ")
                headerTableHeader = "header_" + cleanHeaderKey
                exec(f'''{watchItemHeaderMap[headerTableHeader]} = cleanHeaderValue''')
                dbColumnList.append(watchItemHeaderMap[headerTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[headerTableHeader]})''')

            caseSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > th")
            caseSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(2) > tbody > tr > td")
            for index, caseKey in enumerate(caseSessionKey):
                cleanCaseKey = caseKey.getText().strip('\t\r\n:')
                cleanCaseValue = caseSessionValue[index].getText().strip().replace("\n", " ")
                caseTableHeader = "case_" + cleanCaseKey
                exec(f'''{watchItemHeaderMap[caseTableHeader]} = cleanCaseValue''')
                dbColumnList.append(watchItemHeaderMap[caseTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[caseTableHeader]})''')

            dialSessionKey = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > th")
            dialSessionValue = soup.select("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(1) > table:nth-child(4) > tbody > tr > td")
            for index, dialKey in enumerate(dialSessionKey):
                cleanDialKey = dialKey.getText().strip('\t\r\n:')
                cleanDialValue = dialSessionValue[index].getText().strip().replace("\n", " ")
                dialTableHeader = "dial_" + cleanDialKey
                exec(f'''{watchItemHeaderMap[dialTableHeader]} = cleanDialValue''')
                dbColumnList.append(watchItemHeaderMap[dialTableHeader])
                exec(f'''dbValueList.append({watchItemHeaderMap[dialTableHeader]})''')

            imageUrlSoup = soup.select_one("#watch-detail > div.row > div.col-md-7 > div > div:nth-child(2) > div > picture > img")
            image_url_low_resolution, image_url_high_resolution = None, None
            if imageUrlSoup is not None:
                image_url_low_resolution = imageUrlSoup['src']
                image_url_high_resolution = imageUrlSoup['src'].replace("/lg", "")

            dbColumn = '"' + ('","').join(dbColumnList) + '"'
            placeHolder = ['%s,'] * len(dbColumnList)

            query = f'''INSERT INTO watch_item_info({dbColumn}, "image_url_low_resolution", "image_url_high_resolution", "table_brand", "table_family", "table_key", "info_url") VALUES ({"".join(placeHolder)[:-1]},%s,%s,%s,%s,%s,%s)'''
            cur.execute(query, dbValueList + [image_url_low_resolution, image_url_high_resolution, brand, family, result[0], driver.current_url])
        driver.quit()
        connection.close()
    except Exception as e:
        raise e


def query_from_db_and_download_from_request():
    brand = 'rolex'
    try:
        cur.execute(f'select image_url_high_resolution, table_family, table_key from watch_item_info where table_brand = %s and image_url_high_resolution is not null and image_downloaded is not true', [brand])
        listOfResult = cur.fetchall()
        for result in listOfResult:
            imageUrl = result[0]
            family = result[1]
            key = result[2]
            filename = imageUrl.split("/")[-1]
            errorFromRequest = None
            r = None
            while errorFromRequest is True or errorFromRequest is None:
                try:
                    r = requests.get(imageUrl, stream=True)
                    errorFromRequest = False
                except Exception as e:
                    errorFromRequest = True
                    # rotate_VPN({'opsys': 'Windows', 'command': ['nordvpn', '-c', '-g'], 'settings': ['hong kong'], 'original_ip': '223.16.213.211', 'cwd_path': 'E:\\Program Files\\NordVPN'})

            if r.status_code == 200:
                # Set decode_content value to True, otherwise the downloaded image file's size will be zero.
                r.raw.decode_content = True

                path = f'C:/Users/TANG/Desktop/watch_images/{brand}/{family}/'
                if os.path.isdir(path) is False:
                    os.mkdir(path)
                if '.jpg' in filename:
                    im1 = Image.open(r.raw)
                    filename = filename.replace(".jpg", ".png")
                    im1.save(path + filename)
                else:
                    with open(path + filename, 'wb') as f:
                        shutil.copyfileobj(r.raw, f)

                print('Image successfully Downloaded: ', filename)
                cur.execute(f'update watch_item_info set image_downloaded = true where table_key = %s',
                            [key])
            else:
                print('Image Couldn\'t be retreived')
    except Exception as e:
        raise e


def extractCsv(brand, path):
    try:
        cur.execute(f'select table_header from watch_item_header_map where brand = %s ORDER BY table_header desc', [brand])
        concatHeader = ",".join([r[0] for r in cur.fetchall()])
        productPath = path + brand + r'.csv'
        demoPath = path + brand + r'-demo.csv'
        cur.execute(f"copy (select {concatHeader} from watch_item_info where table_brand = %s) to %s with null as '' CSV HEADER;", [brand, productPath])
        cur.execute(f"copy (select {concatHeader} from watch_item_info where table_brand = %s limit 10) to %s with null as '' CSV HEADER;", [brand, demoPath])
    except Exception as e:
        raise e
