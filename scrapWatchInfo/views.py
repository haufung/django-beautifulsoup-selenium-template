import traceback

from django.http import JsonResponse
# from nordvpn_switcher import initialize_VPN

from .controller import scrapWatchController


# Create your views here.
def healthCheck(request):
    return JsonResponse({"code": 200, "message": "UP"})

def scrapWatchFamilyUrl(request):
    try:
        brand = request.GET['brand']
        data = scrapWatchController.scrapWatchFamilyUrl(brand)
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def scrap_url_to_txt(request):
    try:
        scrapWatchController.scrap_url_to_txt()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful"})

def parse_txt_and_extract(request):
    try:
        scrapWatchController.parse_txt_and_extract()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful"})

def scrapWatchItemUrl(request):
    try:
        brand = request.GET['brand']
        family = None
        if 'family' in request.GET:
            family = request.GET['family']
        data = scrapWatchController.scrapWatchItemUrl(brand, family)
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def scrapWatchItemInfo(request):
    try:
        # settings = initialize_VPN(area_input=['Hong Kong', 'Singapore'])
        # test = request.GET['test']
        brand = request.GET['brand']
        family = None
        if 'family' in request.GET:
            family = request.GET['family']
        data = scrapWatchController.scrapWatchItemInfo(brand, family)
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def scrapWatchItemHeader(request):
    try:
        brand = request.GET['brand']
        data = scrapWatchController.scrapWatchItemHeader(brand)
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def downloadImage(request):
    try:
        # test = request.GET['test']
        data = scrapWatchController.query_from_db_and_download_from_request()
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})

def extractCsv(request):
    try:
        brand = request.GET['brand']
        path = request.GET['path']
        data = scrapWatchController.extractCsv(brand, path)
    except Exception as e:
        print(traceback.format_exc())
        return JsonResponse({"code": 500, "status": "INTERNAL SERVER ERROR", "message": e.args[0]}, status=500)
    return JsonResponse({"code": 200, "status": "SUCCESS", "message": "call successful", "data": data})
